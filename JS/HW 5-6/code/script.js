/*
Створити об'єкт "Документ", де визначити властивості "Заголовок, тіло, футер, дата". 
Створити об'єкт вкладений об'єкт - "Додаток". 
Створити об'єкт "Додаток", вкладені об'єкти, "Заголовок, тіло, футер, дата". 
Створити методи для заповнення та відображення документа.
*/

// let title = prompt("Документ - Вкажіть заголовок");
// let body = prompt("Документ - Вкажіть тіло");
// let footer = prompt("Документ - Вкажіть футер");
// let date = prompt("Документ - Вкажіть дату");

// let addTitle = prompt("Додаток - Вкажіть заголовок");
// let addBody = prompt("Додаток - Вкажіть тіло");
// let addFooter = prompt("Додаток - Вкажіть футер");
// let addDate = prompt("Додаток - Вкажіть дату");

const doc = {
  title: "title",
  body: "body",
  footer: "footer",
  date: "date",
  add: {
    title: { title: "addTitle" },
    body: { body: "addBody" },
    footer: { footer: "addFooter" },
    date: { date: "addDate" },
  },
};

function func() {
  for (let key in doc) {
    if (typeof doc[key] !== "object") {
      document.write(key + ":" + "&nbsp;" + doc[key] + "<br>");
    }
    if (typeof doc[key] == "object") {
      for (let key2 in doc[key]) {
        if (typeof doc[key] == "object") {
          for (let key3 in doc[key][key2]) {
            document.write(
              key2 + ":" + "&nbsp;" + doc[key][key2][key3] + "<br>"
            );
          }
        }
      }
    }
  }
}
func();

/* Напиши функцію map(fn, array), яка приймає на вхід функцію та масив, 
 та обробляє кожен елемент масиву цією функцією, повертаючи новий масив. */

let arr = [1, 2, 3, 4];

function push(array) {
  array.push(5);
  return array;
}

function newFunc(fn, array) {
  let newArr = array.map(function (element) {
    return element * 3;
  });
  return newArr;
}

console.log(push(arr));
console.log(newFunc(push, arr));

/* Створіть сторінку коментарів та стилізуйте її як сторінка відгуків. 
За допомогою методів перебору виведіть на сторінку
 коментарі, пошту, номер коментаря та автора
Дані тут : https://jsonplaceholder.typicode.com/comments */

data.forEach((a, b) => {
  const com = `
    <div>
    <div>#
    ${a.id}
    </div>
    <div>"
    ${a.body}
    "</div>
    <div>
    <span>name:</span>
    ${a.name}
    </div>
    <div>
    <span>email:</span>
    ${a.email}
    </div>
    </div>
    `;

  document.getElementById("root").innerHTML += com;
});
