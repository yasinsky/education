/* ДЗ 1
Використовуючи цикли намалюйте: 
1 Порожній прямокутник
Заповнений 
2 Рівносторонній трикутний 
3 Прямокутний трикутник 
4 Ромб
*/

document.write("ДЗ 1");
document.write("<br>");
document.write("<br>");


// 1 Порожній прямокутник


for (let i = 0; i < 10; i++) {
    document.write("*");
}
for (let i = 0; i < 6; i++) {
    document.write("<br>*");
    for (let k = 0; k < 1; k++) {
        for (let space = 0; space < 16; space++) {
            document.write("&nbsp;")
        }
        document.write("*");
    }
}
document.write("<br>");
for (let i = 0; i < 10; i++) {
    document.write("*");
}

document.write("<hr>");

// Заповнений
// 2 Рівносторонній трикутний 
for (let i = 0; i < 10; ++i) {
    for (let spase = i - i; spase > 0; spase--) {
        document.write("&nbsp;")
    }
    for (let j = 0; j <= i; j++) {
        document.write("*");
    }
    document.write("<br>");
}
document.write("<hr>");


// Заповнений
// 3 Прямокутний трикутник 
for (let i = 0; i < 10; i++) {
    document.write("<br>*");
    for (let k = 0; k < 1; k++) {
        for (let space = 0; space < i; space++) {
            document.write("*")
        }
    }
}
document.write("<hr>");


// Заповнений
// 4 Ромб 
for (let i = 0; i < 5; ++i) {
    for (let spase = (5 - i); spase > 0; spase--) {
        document.write("&nbsp;")
    }
    for (let j = 0; j <= i; j++) {
        document.write("*");
    }
    document.write("<br>");
}
for (let i = 0; i <= 5; i++) {
    for (let spase = (0 + i); spase > 0; spase--) {
        document.write("&nbsp;")
    }
    for (let spase = (0 + i); spase < 6; spase++) {
        document.write("*");
    }
    document.write("<br>");
}
document.write("<hr>");

/*
ДЗ 2
Створіть масив styles з елементами «Джаз» та «Блюз». 
Додайте «Рок-н-рол» до кінця.
Замініть значення всередині на «Класика». 
Ваш код для пошуку значення всередині повинен працювати для масивів з будь-якою довжиною
Видаліть перший елемент масиву та покажіть його.
Вставте «Реп» та «Реггі» на початок масиву.
*/
document.write("ДЗ 2");
document.write("<br>");
document.write("<br>");


const styles = ["Джаз", "Блюз"];
document.write(`Початковий варіант масиву: ${styles}`);
document.write("<br>");
styles.push("Рок-н-рол");
document.write(`Доданий елемент в кінець: ${styles}`);
document.write("<br>");
let arrLenthHalf = Math.floor(styles.length / 2);
styles[arrLenthHalf] = "Класика";
document.write(`Замінений елемент в середині масиву: ${styles}`);
document.write("<br>");
const arr1 = styles.splice(0, 1);
document.write(`Видалений перший елемент масиву: ${arr1}`);
document.write("<br>");
styles.splice(0, 0, "Реп", "Реггі");
document.write(`Фінальний масив: ${styles}`);
