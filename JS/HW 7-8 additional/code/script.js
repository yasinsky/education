/*Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. 
Методи start(), stop(), turnRight(), turnLeft(), 
які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". 
А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна.

Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю.
https://storage.googleapis.com/www.examclouds.com/oop/car-ierarchy.png */

class Driver {
    constructor(fullName, drivingExp) {
        this.fullName = fullName;
        this.drivingExp = drivingExp;
    }
}

class Engine {
    constructor(power, company) {
        this.power = power;
        this.company = company;
    }
}

class Car {
    constructor(model, carClass, weight, driver, engine) {
        this.model = model;
        this.carClass = carClass;
        this.weight = weight;
        this.driver = driver;
        this.engine = engine;
    }

    start() {
        console.dir("Поїхали");
    }

    stop() {
        console.dir("Зупиняємося");
    }

    turnRight() {
        console.dir("Поворот праворуч");
    }

    turnLeft() {
        console.dir("Поворот ліворуч");
    }

    toString() {
        console.dir(`
        Model:${this.model}
        Class:${this.carClass}
        Weight:${this.weight}
        Diver:${this.driver.fullName},${this.driver.drivingExp}
        Engine:${this.engine.power}, ${this.engine.company}
        `);
    }
}

class Lorry extends Car {
    constructor(model, carClass, weight, driver, engine, carrying) {
        super(model, carClass, weight, driver, engine);
        this.carrying = carrying;
    }
    toString() {
        console.dir(`
        Model:${this.model}
        Class:${this.carClass}
        Weight:${this.weight}
        Carrying:${this.carrying}
        Diver:${this.driver.fullName},${this.driver.drivingExp}
        Engine:${this.engine.power}, ${this.engine.company}
        `);
    }
}

class SportCar extends Car {
    constructor(model, carClass, weight, driver, engine, speed) {
        super(model, carClass, weight, driver, engine);
        this.speed = speed;
    }
    toString() {
        console.dir(`
        Model:${this.model}
        Class:${this.carClass}
        Weight:${this.weight}
        Speed:${this.speed}
        Diver:${this.driver.fullName},${this.driver.drivingExp}
        Engine:${this.engine.power}, ${this.engine.company}
        `);
    }
}

const someDriver = new Driver("Lewis Hamilton", "30");
const someEngine = new Engine("380ps", "Jaguar Land Rover");
const someCar = new Car("Jaguar", "SUV", "1800kg", someDriver, someEngine);
someCar.toString();

const someLorry = new Lorry(
    "Jaguar",
    "SUV",
    "1800kg",
    someDriver,
    someEngine,
    "0.4tons"
);
someLorry.toString();

const someSportCar = new SportCar(
    "Jaguar",
    "SUV",
    "1800kg",
    someDriver,
    someEngine,
    "280km/h"
);
someSportCar.toString();
