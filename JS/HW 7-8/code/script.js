/*
Створіть клас Phone, який містить змінні number, model і weight.
Створіть три екземпляри цього класу.
Виведіть на консоль значення їх змінних.
Додати в клас Phone методи: receiveCall, має один параметр - ім'я.
Виводить на консоль повідомлення "Телефонує {name}".
Метод getNumber повертає номер телефону. 
Викликати ці методи кожного з об'єктів.

*/

class Phone {
    constructor(number, model, weight) {
        this.number = number;
        this.model = model;
        this.weight = weight;
    }
    receiveCall(name) {
        return (this.name = name);
    }
}

function getNumber(obj) {
    return obj.number;
}

const phone1 = new Phone("+3809311122233", "13 mini", "138");
const phone2 = new Phone("+3806344422255", "13 Pro", "160");
const phone3 = new Phone("+3805077722288", "13 Pro Max", "183");
phone1.receiveCall("Gilbert");
phone2.receiveCall("Alisha");
phone3.receiveCall("Matthew");

console.log(`Телефонує ${phone1.name}`);
console.log(`Телефонує ${phone2.name}`);
console.log(`Телефонує ${phone3.name}`);

console.dir(getNumber(phone1));
console.dir(getNumber(phone2));
console.dir(getNumber(phone3));

/*- Написати функцію filterBy(), яка прийматиме 2 аргументи.
Перший аргумент - масив, який міститиме будь-які дані,
другий аргумент - тип даних.

- Функція повинна повернути новий масив,
який міститиме всі дані, які були передані в аргумент,
за винятком тих, тип яких був переданий другим аргументом.
Тобто якщо передати масив ['hello', 'world', 23, '23', null],
і другим аргументом передати 'string',
то функція поверне масив [23, null]. */

const arr1 = ["hello", "world", 23, "23", null];
const dataType = "string"; // typeof null === 'object', цікавий момент, багато гуглив)

function filterBy(a, b) {
    let filteredArr = [];

    // a.forEach(function (i) {
    //     if (typeof i !== b) {
    //         filteredArr.push(i);
    //     }
    // });

    // Стрілкова функція з тернарним if, невеликий квест для мене був. Не знаю, чи так правильно (чи читається), але практика)

    a.forEach(( (i) => {typeof i !== b ? filteredArr.push(i) : false}));
    return filteredArr;
}

console.log(filterBy(arr1, dataType));
